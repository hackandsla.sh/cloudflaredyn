package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/miekg/dns"
	"github.com/rifflock/lfshook"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Execute the root command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var rootCmd = &cobra.Command{
	Use:     "cloudflaredyn",
	Short:   "cloudflaredyn is an app to help dynamically update Cloudflare DNS records",
	Long:    "cloudflaredyn is an app to help dynamically update Cloudflare DNS records",
	Version: "0.1.2",
	Run: func(cmd *cobra.Command, args []string) {
		viper.Unmarshal(&rootConfig)
		_, err := govalidator.ValidateStruct(&rootConfig)
		if err != nil {
			log.Fatal("error: " + err.Error())
		}

		fileHook := lfshook.NewHook(
			lfshook.PathMap{},
			&log.TextFormatter{},
		)
		fileHook.SetDefaultPath(rootConfig.LogFile)
		log.AddHook(fileHook)

		if rootConfig.Verbose {
			log.SetLevel(log.DebugLevel)
		}

		// Validate that the given token is valid
		result := &cfValidTokenResponse{}
		err = getCloudflareAPIResponse(rootConfig, "/user/tokens/verify", "GET", nil, result)
		if err != nil {
			log.Fatal("error while validating token: " + err.Error())
		}
		if result.Result.Status != "active" {
			log.WithFields((log.Fields{
				"status": result.Result.Status,
			})).Fatal("token status is not active")
		} else {
			log.WithFields((log.Fields{
				"status": "active",
			})).Debug("API token check succeeded")
		}

		// Get the current external IP. The current methodology is via the myip.opendns.com
		externalIP := ""
		target := "myip.opendns.com"
		server := "208.67.222.222"

		c := dns.Client{}
		m := dns.Msg{}
		m.SetQuestion(target+".", dns.TypeA)
		r, t, err := c.Exchange(&m, server+":53")
		if err != nil {
			log.Fatal(err)
		}
		log.WithFields((log.Fields{
			"time": t,
		})).Debug("completed DNS lookup")
		if len(r.Answer) == 0 {
			log.Fatal("No results from DNS lookup")
		}
		externalIP = r.Answer[0].(*dns.A).A.String()
		log.WithFields((log.Fields{
			"ip": externalIP,
		})).Info("external IP found")

		// Get the record ID for the desired record
		recordResult := &cfRecordResponse{}
		path := "/zones/" + rootConfig.Zone + "/dns_records?type=A&name=" + rootConfig.Record
		err = getCloudflareAPIResponse(rootConfig, path, "GET", nil, recordResult)
		if err != nil {
			log.Fatal("error while validating token: " + err.Error())
		}
		if recordResult.Success != true {
			logFields := log.Fields{}
			for i, zoneErr := range recordResult.Errors {
				msg := zoneErr.Message
				logFields["error"+strconv.Itoa(i)] = msg
			}
			log.WithFields(logFields).Fatal("record lookup not successful")
		} else {
			log.WithFields(log.Fields{
				"name":    recordResult.Result[0].Name,
				"id":      recordResult.Result[0].ID,
				"content": recordResult.Result[0].Content,
			}).Debug("record lookup successful")
		}
		recordID := recordResult.Result[0].ID
		currentIP := recordResult.Result[0].Content
		logFields := log.Fields{
			"currentIp": currentIP,
			"newIp":     externalIP,
		}

		// Update the record if needed
		if externalIP != currentIP {
			if rootConfig.DryRun {
				log.WithFields(logFields).Info("dry-run: would update record")
			} else {
				updateRecordResult := &cfRecordUpdateResponse{}
				path = "/zones/" + rootConfig.Zone + "/dns_records/" + recordID
				body := cfRecordRequest{
					Name:    rootConfig.Record,
					Content: externalIP,
					Type:    "A",
				}
				err = getCloudflareAPIResponse(rootConfig, path, "PUT", body, updateRecordResult)
				if err != nil {
					log.Fatal("error while updating record: " + err.Error())
				}
				if updateRecordResult.Success != true {
					logFields := log.Fields{}
					for i, zoneErr := range updateRecordResult.Errors {
						msg := zoneErr.Message
						logFields["error"+strconv.Itoa(i)] = msg
					}
					log.WithFields(logFields).Fatal("record update not successful")
				}
				log.WithFields(logFields).Info("updating record")
			}
		} else {
			log.WithFields(logFields).Info("no update: old and new IPs match")
		}
	},
}

type rootConfigStruct struct {
	Token   string `valid:"alphanum~'token' must be alphanumeric, required~'token' must be specified, stringlength(40|40)~'token' must be exactly 40 characters long"`
	Zone    string `valid:"alphanum~'zone' must be alphanumeric, required~'zone' must be specified, stringlength(32|32)~'zone' must be exactly 32 characters long"`
	Record  string `valid:"dns~'record' must be a valid DNS name, required~'record' must be specified"`
	DryRun  bool   `mapstructure:"dry-run"`
	Verbose bool
	LogFile string `mapstructure:"log-file"`
}

var rootConfig rootConfigStruct

var customConfigFile string

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&customConfigFile, "config", "", "config file")

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	rootCmd.PersistentFlags().StringP("token", "t", "", "The Cloudflare token to use to access the API")
	viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))

	rootCmd.PersistentFlags().StringP("zone", "z", "", "The Cloudflare zone ID that contains the DNS record to update")
	viper.BindPFlag("zone", rootCmd.PersistentFlags().Lookup("zone"))

	rootCmd.PersistentFlags().StringP("record", "r", "", "The DNS record to update")
	viper.BindPFlag("record", rootCmd.PersistentFlags().Lookup("record"))

	rootCmd.PersistentFlags().BoolP("dry-run", "d", false, "if specified, will not actually update records")
	viper.BindPFlag("dry-run", rootCmd.PersistentFlags().Lookup("dry-run"))

	rootCmd.PersistentFlags().BoolP("verbose", "v", false, "if specified, will run verbosely")
	viper.BindPFlag("verbose", rootCmd.PersistentFlags().Lookup("verbose"))

	rootCmd.PersistentFlags().StringP("log-file", "l", "/var/log/cloudflaredyn.log", "if specified, will run verbosely")
	viper.BindPFlag("log-file", rootCmd.PersistentFlags().Lookup("log-file"))
}

func initConfig() {
	// Don't forget to read config either from cfgFile or from home directory!
	if customConfigFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(customConfigFile)
		if err := viper.ReadInConfig(); err != nil {
			fmt.Println("Can't read config:", err)
			os.Exit(1)
		}
	} else {
		viper.SetConfigName("cloudflaredyn-config")        // name of config file (without extension)
		viper.AddConfigPath(".")                           // optionally look for config in the working directo
		viper.AddConfigPath("$HOME/.config/cloudflaredyn") // call multiple times to add many search paths
		viper.AddConfigPath("/etc/cloudflaredyn/")         // path to look for the config file in
		viper.ReadInConfig()                               // Find and read the config file
	}
}

func getCloudflareAPIResponse(config rootConfigStruct, path string, method string, body interface{}, item interface{}) error {
	rootURL := "https://api.cloudflare.com/client/v4"
	url := rootURL + path
	var jsonBody []byte
	if body != nil {
		var err error
		jsonBody, err = json.Marshal(body)
		if err != nil {
			return err
		}
	} else {
		jsonBody = nil
	}

	client := &http.Client{}
	req, _ := http.NewRequest(method, url, bytes.NewBuffer(jsonBody))
	req.Header.Set("Authorization", "Bearer "+config.Token)
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	return json.NewDecoder(res.Body).Decode(item)
}

type cfValidTokenResponse struct {
	Result struct {
		Status string `json:"status"`
	} `json:"result"`
}

type cfRecordResponse struct {
	Success bool              `json:"success"`
	Errors  []cfErrorResponse `json:"errors"`
	Result  []struct {
		ID      string `json:"id"`
		Name    string `json:"name"`
		Content string `json:"content"`
	} `json:"result"`
}

type cfErrorResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type cfRecordUpdateResponse struct {
	Success bool              `json:"success"`
	Errors  []cfErrorResponse `json:"errors"`
	Result  struct {
		ID      string `json:"id"`
		Name    string `json:"name"`
		Content string `json:"content"`
	} `json:"result"`
}

type cfRecordRequest struct {
	Type    string `json:"type"`
	Name    string `json:"name"`
	Content string `json:"content"`
}
