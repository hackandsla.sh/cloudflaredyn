package main

import (
	"gitlab.com/hackandsla.sh/cloudflaredyn/cmd"
)

func main() {
	cmd.Execute()
}
