
# cloudflaredyn

![Logo](media/logo.png)

> A simple program to dynamically update Cloudflare DNS records

This program will find your external IP, and then synchronize a specified Cloudflare A record to match up with it.

This is essentially DynDNS for Cloudflare. It is similar to [CloudFlare-DDNS-Updater](https://github.com/birkett/CloudFlare-DDNS-Updater), but will work on other systems besides Windows.

## Installation

To get a pre-compiled binary for your architecture, just head over to the [releases](https://gitlab.com/hackandsla.sh/cloudflaredyn/-/releases) page.

If you would like to compile it yourself:

```bash
go get gitlab.com/hackandsla.sh/cloudflaredyn
```

## Usage

1. Create a Cloudflare API token by going to **My Profile > API Tokens > Create Token**

2. Create a new API token with the following fields:

    * **Token Name**: `cloudflaredyn token`
    * **Permissions**: `Zone, DNS, Edit`
    * **Zone Resources**: `Include, specific zone, < The zone you want to use >`

3. Get the Zone ID by going to the home page for the zone you wish to edit, and copying the **Zone ID** field

4. Create a configuration file called `cloudflaredyn-config.yaml` with the following:

    ```yaml
    token: < your API key from step 2 >
    zone: < your zone from step 3 >
    record: < the record you wish to keep updated >
    ```

5. Put this configuration file in one of the following places:

    * `/etc/cloudflaredyn` - For system-wide config
    * `~/.config/cloudflaredyn` - For user-level config

6. Run the cloudflaredyn tool

    ```bash
    cloudflaredyn
    ```

## Automated usage

To run the tool on a regular schedule, you can use a cron job like the following to run the tool every 10 minutes:

```cron
*/10 * * * * cloudflaredyn
```

## Docker usage

To run the tool with Docker, just run the latest version of the `terrabitz/cloudflaredyn` image:

```bash
docker run \
    -e TOKEN=<API token> \
    -e ZONE=<Zone ID> \
    -e RECORD=<record to update>\
    terrabitz/cloudflaredyn
```

## Help docs

```none
> cloudflaredyn -h
cloudflaredyn is an app to help dynamically update Cloudflare DNS records

Usage:
  cloudflaredyn [flags]

Flags:
      --config string     config file
  -d, --dry-run           if specified, will not actually update records
  -h, --help              help for cloudflaredyn
  -l, --log-file string   if specified, will run verbosely (default "/var/log/cloudflaredyn.log")
  -r, --record string     The DNS record to update
  -t, --token string      The Cloudflare token to use to access the API
  -v, --verbose           if specified, will run verbosely
      --version           version for cloudflaredyn
  -z, --zone string       The Cloudflare zone ID that contains the DNS record to update
```

## Meta

Trevor Taubitz – [@terrabitz](https://twitter.com/terrabitz)

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/terrabitz/](https://gitlab.com/terrabitz)
